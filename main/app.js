
function distance(first, second){
	//TODO: implementați funcția
	// TODO: implement the function
	var dist =0;
	if(Array.isArray(first) === true && Array.isArray(second)===true){
		if(first.length >0 && second.length > 0 && 
			typeof first !== 'undefined' &&
		typeof second !== 'undefined'){
			var unique1 = Array.from(new Set(first));
			var unique2 = Array.from(new Set(second));
			for(var i =0 ;i<unique1.length;i++){
				for(var j=0;j<unique2.length;j++){
					if(unique1[i] !== unique2[j] ){
						dist++;
					} 
				}
			}
		} else {
			return 0;
		}
		return dist;
	} else {
		throw new Error('InvalidType');
	}

}


module.exports.distance = distance